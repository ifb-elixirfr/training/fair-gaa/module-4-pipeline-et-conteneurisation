# Module 4 - Pipeline et conteneurisation

## Description Cours Magistral (CM)

⏱️ Durée : 2h

📑 Plan :
- Présentation de la conteneurisation (Docker/Singularity/Conda) et de leur intérêt dans un contexte FAIR. Utilisation des outils pour la récupération/installation:
  - Conda create
  - Singularity build depuis un docker
- Présentation des pipelines et de leur intérêt dans un contexte FAIR. Présentation des principaux moteurs de pipeline en bioinfo:
  - Galaxy
  - SnakeMake
  - Nextflow

📚 Supports :
- [Slides](https://ifb-elixirfr.gitlab.io/training/fair-gaa/module-4-pipeline-et-conteneurisation)

## Descritpion Travaux Pratiques (TP)

⏱️ Durée : 3h + 3h

📑 Plan :
- Installation des outils de manière atomique avec Conda et Singularity
- Utilisation d’un même pipeline avec 3 moteurs différents (G/S/N) -> reproductibilité
- Edition et utilisation d’un pipeline SnakeMake
- Conversion du pipeline en Nextflow

📚 Supports :
- [Notebook Tool packaging and container](https://gitlab.com/ifb-elixirfr/training/notebooks/tool-packaging-and-container)
- [Workflows](https://gitlab.com/ifb-elixirfr/training/workflows)


